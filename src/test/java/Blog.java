import com.github.javafaker.Faker;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.locators.RelativeLocator;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;

public class Blog {
    public static void main(String[] args) throws Exception {
        //set property
        System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe");
        //added authorization
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("---remote-allow-origins=*");
        //opened chrome instance
        WebDriver driver = new ChromeDriver(chromeOptions);
        Actions actions = new Actions(driver);

        //for maximizing the screen on own
        driver.manage().window().maximize();

        ///implicit wait-specifically agar check krna hai itne time baad yeh aaya ki ni ya kya
//        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));

        //explicit wait
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));

        //added url to be checked
        driver.get("https://staging.studylinkclasses.com/login");

        //checked if email and password input is displayed
        boolean isEmailInputDisplayed = driver.findElement(By.name("email")).isDisplayed();
        boolean isPassswordInputDisplayed = driver.findElement(By.name("password")).isDisplayed();

        //if not throw an exception
        if(!(isEmailInputDisplayed && isPassswordInputDisplayed)){
            throw new Exception("Email or Password input not visible");
        }

        WebElement loginSubmitButton= driver.findElement(By.id("kt_login_signin_submit"));
        if(!loginSubmitButton.isDisplayed())
        {
            throw  new Exception("Submit button for login page is not displayed");
        }
        //clciked login button
       loginSubmitButton.click();

        //checkd if error and password error is being displayed on clicking directly without filling the data
        boolean isEmailErrorMessageDisplayed = driver.findElement(By.xpath("//strong[normalize-space()='The email field is required.']")).isDisplayed();
        boolean isPasswordErrorMessageDisplayed = driver.findElement(By.xpath("//strong[normalize-space()='The password field is required.']")).isDisplayed();

        //if not throw an exception
        if(!(isEmailErrorMessageDisplayed && isPasswordErrorMessageDisplayed)){
            throw new Exception("Email or Password error message not displayed");
        }

        //invalid email credentials
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("author19studylinkclasses.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(("abcd1234"));
        driver.findElement(By.id("kt_login_signin_submit")).click();
        boolean isInvalidEmailCredentialDisplayed = driver.findElement(By.xpath("//strong[normalize-space()='These credentials do not match our records.']")).isDisplayed();

        if(!isInvalidEmailCredentialDisplayed)
        {
            throw new Exception("email credentials invalid Re-enter");
        }

        driver.findElement(By.xpath("//input[@name='email']")).clear();
        driver.findElement(By.xpath("//input[@name='password']")).clear();


        //invalid password credentials
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("author19@studylinkclasses.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(("acd1234"));
        driver.findElement(By.id("kt_login_signin_submit")).click();
        boolean isInvalidPasswordCredentialDisplayed = driver.findElement(By.xpath("//strong[normalize-space()='These credentials do not match our records.']")).isDisplayed();

        if(!isInvalidPasswordCredentialDisplayed)
        {
            throw new Exception("email credentials invalid Re-enter");
        }

        driver.findElement(By.xpath("//input[@name='email']")).clear();
        driver.findElement(By.xpath("//input[@name='password']")).clear();

        //login with email and without password
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("author19@studylinkclasses.com");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys((""));
        driver.findElement(By.id("kt_login_signin_submit")).click();
       boolean  isMissingPasswordMsgDisplayed = driver.findElement(By.xpath("//strong[normalize-space()='The password field is required.']")).isDisplayed();

        if(!isMissingPasswordMsgDisplayed)
        {
            throw new Exception("Password missing");
        }

        driver.findElement(By.xpath("//input[@name='email']")).clear();
        driver.findElement(By.xpath("//input[@name='password']")).clear();



        //login without email
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys("");
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(("abcd1234"));
        driver.findElement(By.id("kt_login_signin_submit")).click();
        boolean  isMissingEmailMsgDisplayed = driver.findElement(By.xpath("//strong[normalize-space()='The email field is required.']")).isDisplayed();

        if(!isMissingEmailMsgDisplayed)
        {
            throw new Exception("email missing");
        }

        driver.findElement(By.xpath("//input[@name='email']")).clear();
        driver.findElement(By.xpath("//input[@name='password']")).clear();


        //checked using entering details of email,password, checked the box of remember me and clciked login button
        loginAndValidate("author19@studylinkclasses.com", "abcd1234", driver);


        //Locating Sidebar links with relative locators
        WebElement siteLogo = driver.findElement(By.id("kt_header_brand"));
        WebElement dashboardLink = driver.findElement(RelativeLocator.with(By.tagName("a")).below(siteLogo));
        boolean isDashboardLinkVisible = dashboardLink.isDisplayed();
        if (!isDashboardLinkVisible){
            throw new Exception("Dashboard sidebar link is not visible");
        }

        WebElement blogsLink = driver.findElement(RelativeLocator.with(By.tagName("a")).below(dashboardLink));
        boolean isBlogsLinkVisible = blogsLink.isDisplayed();
        if(!isBlogsLinkVisible){
            throw new Exception("Blogs sidebar link is not visible");
        }

        blogsLink.click();
        String blogsPageURL = driver.getCurrentUrl();
        if(!blogsPageURL.equals("https://staging.studylinkclasses.com/control-panel/blogs")){
            throw new Exception("Redirected to wrong page after clicking on blogs sidebar link");
        }

        boolean isBlogsTitleVisible = driver.findElement(By.xpath("//h3[normalize-space()='Manage Blogs']")).isDisplayed();
        if(!isBlogsTitleVisible){
            throw new Exception("Manage blogs title is not visible on blogs index page");
        }

        //Creating blog
        driver.findElement(By.xpath("//a[normalize-space()='Create Blog']")).click();

        WebElement blogTitleInput = driver.findElement(By.name("title"));
        WebElement blogExcerptInput = driver.findElement(By.name("excerpt"));
      //  WebElement blogBodyInput = driver.findElement(By.id("tinymce"));
        WebElement blogTagsSearchInput = driver.findElement(By.xpath("//input[@placeholder='Search']"));
        WebElement blogTagsAddButton = driver.findElement(By.xpath("//div[contains(@class,'dual-listbox__buttons')]//button[normalize-space()='Add']"));
        WebElement blogImageInput = driver.findElement(By.                                                                                                                                                                                        name("image"));
        WebElement blogPublishedAt = driver.findElement(By.name("published_at"));
        WebElement blogSEODescription = driver.findElement(By.name("seo_description"));
        WebElement blogSEOKeywords = driver.findElement(By.name("seo_keywords"));
        WebElement blogSEOOGTitle = driver.findElement(By.name("seo_og_title"));
        WebElement blogSEOOGDescription = driver.findElement(By.name("seo_og_description"));
        WebElement createBlogSubmitButton = driver.findElement(By.xpath("//button[@type='submit']"));

        if(!blogTitleInput.isDisplayed()){
            throw new Exception("Title input is not visible");
        }

        if(!blogExcerptInput.isDisplayed()){
            throw new Exception("Blog Excerpt input is not visible");
        }
//        if(!blogBodyInput.isDisplayed()){
//            throw new Exception("Blog body input is not visible");
//        }
        if(!blogTagsSearchInput.isDisplayed()){
            throw new Exception("Blog search input is not visible");
        }
        if(!blogTagsAddButton.isDisplayed()){
            throw new Exception("Blog add button is not visible");
        }
//        if(!blogImageInput.isDisplayed()){
//            throw new Exception("Blog Image input is not visible");
//        }
        if(!blogPublishedAt.isDisplayed()){
            throw new Exception("Blog Publish At input is not visible");
        }
        if(!blogSEODescription.isDisplayed()){
            throw new Exception("Blog SEO description input is not visible");
        }
        if(!blogSEOKeywords.isDisplayed()){
            throw new Exception("Blog SEO keywords input is not visible");
        }
        if(!blogSEOOGTitle.isDisplayed()){
            throw new Exception("Blog SEO OG Title input is not visible");
        }
        if(!blogSEOOGDescription.isDisplayed()){
            throw new Exception("Blog SEO OG Description input is not visible");
        }
        if(!createBlogSubmitButton.isDisplayed()){
            throw new Exception("Create Blog Submit button is not visible");
        }


        //Blog data
        Faker faker = new Faker();

        HashMap<String, Object> blogData = new HashMap<>();

//        String fakeTitle = faker.book().title();
        String fakeTitle = faker.lorem().sentence();
        blogTitleInput.sendKeys(fakeTitle);
        blogData.put("title",fakeTitle);

        String fakeExcerpt = faker.lorem().sentence(15);
        blogExcerptInput.sendKeys(fakeExcerpt);
        blogData.put("excerpt", fakeExcerpt);

        driver.switchTo().frame("blog-body_ifr");

        String fakeBodyText = faker.lorem().paragraph(20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("body")));
        WebElement iframeBody = driver.findElement(By.cssSelector("body"));
        iframeBody.sendKeys(fakeBodyText);
        blogData.put("body",fakeBodyText);

//will throw error if not written control is not taken from iframe
        driver.switchTo().parentFrame();

        //cant repeat code again and again for another li
//        blogTagsSearchInput.sendKeys("laravel");
//       driver.findElement(By.xpath("//ul[@class='dual-listbox__available']/li[normalize-space()='laravel']")).click();
//
//        blogTagsAddButton.click();
//        String selectedTagText1 = driver.findElement(By.xpath("\"//ul[@class='dual-listbox__selected']/li[1]\"")).getText();
//
//        if(!selectedTagText1.equals("laravel")){
//            throw new Exception("Dual list not working");
//        }

        String[] blogTags = {"laravel", "coding", "php"};

        //checking if selected tag name is added om clicking add buttton
        for(int i = 0; i<blogTags.length; i++){
            blogTagsSearchInput.sendKeys(blogTags[i]);
            driver.findElement(By.xpath("//ul[@class='dual-listbox__available']/li[normalize-space()='"+blogTags[i]+"']")).click();
            blogTagsAddButton.click();
            blogTagsSearchInput.clear();
            String selectedTagName = driver.findElement(By.xpath("//ul[@class='dual-listbox__selected']/li["+(i+1)+"]")).getText();
            if(!selectedTagName.equals(blogTags[i])){
                throw new Exception("Dual list of tags select not working");
            }
        }
        blogData.put("tags",blogTags);

        //image i/p
        File f = new File("src/test/resources/images/laravel.jpg");
        String imagePath = f.getAbsolutePath();
        System.out.println("Image path: " +imagePath);
        blogImageInput.sendKeys(imagePath);


        //handling date-time i/p in blog published at
        LocalDateTime localDateTimeObj = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("YYYY-MM-dd HH:mm");
        String formattedDate = localDateTimeObj.format(formatter);
        blogPublishedAt.sendKeys(formattedDate);
        blogData.put("published_at",formattedDate);

        String fakeSEODescription = faker.lorem().sentence(12);
        blogSEODescription.sendKeys(fakeSEODescription);
        blogData.put("seo_description", fakeSEODescription);

        String fakeSEOKeywords = faker.lorem().sentence(8).replace("",",");
        blogSEOKeywords.sendKeys(fakeSEOKeywords);
        blogData.put("seo_keywords", fakeSEOKeywords);

        String fakeOGTitle = faker.lorem().sentence();
        blogSEOOGTitle.sendKeys(fakeOGTitle);
        blogData.put("seo_og_title",fakeOGTitle);

        String fakeOGDescription =  faker.lorem().sentence();
        blogSEOOGDescription.sendKeys(fakeOGDescription);
        blogData.put("seo_og_description",fakeOGDescription);
        createBlogSubmitButton.click();

        boolean isBlogCreatedSuccessfully = driver.findElement(By.xpath("//div[contains(text(),'Blog was created Successfully')]")).isDisplayed();
        if(!isBlogCreatedSuccessfully)
        {
            throw new Exception("There is problem in  blog creation");
        }

        //Searching the blog added
        WebElement datatableSearchInput = driver.findElement(By.xpath("//input[@type='search']"));
        datatableSearchInput.sendKeys((String)blogData.get("title"));







        //validate table details
//         datatableSearchInput = driver.findElement(By.xpath("//input[@type='search']"));
//        datatableSearchInput.sendKeys((String)blogData.get("title"));

        Thread.sleep(3000);
        WebElement ValidateTitle = driver.findElement(By.xpath("//table//tbody/tr/td[normalize-space()='"+(String) blogData.get("title")+"']/parent::tr/td[3]"));
        String titleToBeValidated = ValidateTitle.getText();
        if(!titleToBeValidated.equals((String) blogData.get("title")))
        {
            throw new Exception("Title mismatched at validating table row ");
        }

        driver.findElement(By.xpath("//div[@class='kt-header__topbar-wrapper']//img[@alt='Pic']")).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/div[@class='kt-user-card__name']")));
        WebElement author = driver.findElement(By.xpath("//div[@class='kt-user-card__name']"));
        String authorName = author.getText();
        driver.findElement(By.xpath("//div[@class='kt-header__topbar-wrapper']//img[@alt='Pic']")).click();

        //Validate Author
        WebElement ValidateAuthor =
                driver.findElement(By.xpath(
                        "//table//tbody/tr/td[4]"));
        String authorToBeValidated =
                ValidateAuthor.getText();
        if(!authorToBeValidated.equals(authorName))
        {
            throw new Exception("Author mismatched at validating table row ");
        }

        //Validate  tags
        WebElement ValidateTags = driver.findElement(By.xpath("//table//tbody/tr/td[6]"));
        String[] tagsToBeValidated = ValidateTags.getText().split(",");

        String[] ogBlogTags = (String[])blogData.get("tags");

//        for(int i=0; i<ogBlogTags.length; i++){
//            System.out.println(ogBlogTags[i]);
//        }
//        for(int i=0; i<tagsToBeValidated.length; i++){
//            System.out.println(tagsToBeValidated[i]);
//        }
        if(ogBlogTags.length != tagsToBeValidated.length){
            throw new Exception("Tags count mismatched");
        }

        for(int i = 0; i< ogBlogTags.length;i++){
            if(!((tagsToBeValidated[i]).equals(ogBlogTags[i]))){
                System.out.println(ogBlogTags[i]);
                System.out.println(tagsToBeValidated[i]);
                throw new Exception("One of tag mismatched");
            }
        }




//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[1]//i[@class='kt-menu__link-icon la la-eye']")));
//        driver.findElement(By.xpath("//a[1]//i[@class='kt-menu__link-icon la la-eye']")).click();




        //Validating Blog Details
       // validateBlogDetails(driver,blogData);


/*
        //3rd lect
        //admin side
        driver.navigate().back();

        driver.findElement(By.xpath("//div[@class='kt-header__topbar-wrapper']//img[@alt='Pic']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[normalize-space()='Sign Out']")));
        driver.findElement(By.xpath("//a[normalize-space()='Sign Out']")).click();

        driver.get("https://staging.studylinkclasses.com/login");

        loginAndValidate("admin19@studylinkclasses.com", "abcd1234", driver);

        //Clicking manage blogs
        WebElement sidebarBlogsLink = driver.findElement(By.xpath("//a[@href='https://staging.studylinkclasses.com/control-panel/blogs']"));
        actions.moveToElement(sidebarBlogsLink);
        actions.perform();
        sidebarBlogsLink.click();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='search']")));
        driver.findElement(By.xpath("//input[@type='search']")).sendKeys((String)blogData.get("title"));


        Thread.sleep(5000);

        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[1]//i[@class='kt-menu__link-icon la la-eye']")));
        driver.findElement(By.xpath("//a[1]//i[@class='kt-menu__link-icon la la-eye']")).click();

        validateBlogDetails(driver,blogData);

        driver.get("https://staging.studylinkclasses.com/blogs");

        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("preloader")));

        driver.findElement(By.xpath("//input[@placeholder='Search Blogs']")).sendKeys((String)blogData.get("title"), Keys.ENTER);

        String searchedBlogCount = driver.findElement(By.xpath("//div[@class='row align-items-center mb-3']//div[contains(text(),'We found')][1]/strong[1]")).getText();

        if(!searchedBlogCount.equals("0")){
            List<WebElement> blogCards = driver.findElements(By.xpath("//div[@class='articles_grid_style']//div[@class='articles_grid_caption']/h4/a"));

            for(int i = 0; i<blogCards.size(); i++){
                String blogTitle = blogCards.get(i).getText().trim();

                if(blogTitle.equalsIgnoreCase(((String)blogData.get("title")))){
                    throw new Exception("Unapproved blog is visible on public page");
                }
            }

//            throw new Exception("Blog is publibly visible without approving!!");
        }


       driver.get("https://staging.studylinkclasses.com/control-panel/blogs");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='search']")));
        driver.findElement(By.xpath("//input[@type='search']")).sendKeys((String)blogData.get("title"));

        Thread.sleep(3000);

        driver.findElement(By.xpath("//table//tbody/tr/td[normalize-space()='"+(String) blogData.get("title")+"']/parent::tr/td[9]/div/a/i[@class='kt-menu__link-icon flaticon2-check-mark']")).click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("approveModal")));
        boolean isCloseButtonofModalVisible = driver.findElement(By.xpath("//div[@id='approveModal']//button[@type='button'][normalize-space()='Close']")).isDisplayed();

        boolean isApproveButtonOfModalVisible = driver.findElement(By.xpath("//div[@id='approveModal']//button[@type='submit'][normalize-space()='Approve']")).isDisplayed();


        if(!(isCloseButtonofModalVisible && isApproveButtonOfModalVisible)){
            throw new Exception("Approve or close button of approve blog modal is not visible");
        }

        driver.findElement(By.xpath("//div[@id='approveModal']//button[@type='submit'][normalize-space()='Approve']")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[normalize-space()='Blog was approved Successfully'][@class='alert-text']")));

        //homework


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@type='search']")));
        driver.findElement(By.xpath("//input[@type='search']")).sendKeys((String)blogData.get("title"));

        Thread.sleep(3000);

        boolean isDisapproveButtonVisible = driver.findElement(By.xpath("//table//tbody/tr/td[normalize-space()='"+(String) blogData.get("title")+"']/parent::tr/td[9]/div/a[normalize-space()='Disapprove']")).isDisplayed();
        if(!isDisapproveButtonVisible)
        {
            throw new Exception("Disapprove button is not visible after approving a blog");
        }


//        WebElement element = driver.findElement(By.xpath(""));
//        JavascriptExecutor js = (JavascriptExecutor) driver;
//        js.executeScript("arguments[0].innerHTML='Hello'",element);

        


//        driver.quit();

 */
    }

    static void validateBlogDetails(WebDriver driver, HashMap<String, Object>blogData) throws Exception {
        String extractedTitle = driver.findElement(By.xpath("//h1[normalize-space()='"+blogData.get("title")+"']")).getText();
        if(!extractedTitle.equalsIgnoreCase((String)blogData.get("title") )){
            throw  new Exception("Blog title msimatched in preview");
        }

        String extracetedBreadcrumbText = driver.findElement(By.xpath("//li[@class='breadcrumb-item active']")).getText();
        if(!extracetedBreadcrumbText.equals((String) blogData.get("title"))){
            throw new Exception("Breadcrumb text mismatched");
        }

        String extractedPostTitle = driver.findElement(By.xpath("//h2[@class='post-title']")).getText();
        if(!extractedPostTitle.equalsIgnoreCase((String) blogData.get("title"))){
            throw new Exception("Post title mismatched");
        }

        String extractedBody = driver.findElement(By.xpath("//div[@class='article_body_wrap']/p[1]")).getText();
        if(!extractedBody.equals((String) blogData.get("body"))){
            throw new Exception("Blog body mismatched");
        }

        List<WebElement> extractedPostTags = driver.findElements(By.xpath("//div[@class='article_bottom_info']/div[@class='post-tags']/ul/li"));
        String[] ogBlogTags = (String[])blogData.get("tags");
        if(ogBlogTags.length != extractedPostTags.size()){
            throw new Exception("Tags count mismatched");
        }

        for(int i = 0; i< ogBlogTags.length;i++){
            if(!extractedPostTags.get(i).getText().equals(ogBlogTags[i])){
                throw new Exception("One of tag mismatched");
            }
        }
    }

    static void loginAndValidate(String username, String password, WebDriver driver) throws Exception {
        driver.findElement(By.xpath("//input[@name='email']")).sendKeys(username);
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys((password));

        WebElement checkboxOfRememberMe = driver.findElement(By.xpath("//label[normalize-space()='Remember me']"));

        if(!checkboxOfRememberMe.isDisplayed())
        {
            throw new Exception("Checkbox for remember me is not displayed");
        }

        checkboxOfRememberMe.click();
        driver.findElement(By.id("kt_login_signin_submit")).click();

        //validating successful login
        String dashboardURL = driver.getCurrentUrl();
        if(!dashboardURL.equals("https://staging.studylinkclasses.com/control-panel/dashboard")){
            throw  new Exception("Redirected to wrong page after login!");
        }

        //Visibility of dashboard title
        boolean isDashboardTitleVisible = driver.findElement(By.xpath("//h3[normalize-space()='Study Link -Admin Panel']")).isDisplayed();
        if(!isDashboardTitleVisible){
            throw new Exception("Dashbaord title is not visible");
        }


    }

}


